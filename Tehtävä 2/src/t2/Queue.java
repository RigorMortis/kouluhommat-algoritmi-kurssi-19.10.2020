package t2;

public class Queue {
    private int mSize; // pinottujen alkioiden lkm
    private ListItem mHead;
    private ListItem mTail;
    
    // luo uusi lista-alkio, vie se pinon huipulle
    public void push(String aData){
       ListItem newItem = new ListItem(); //initialisoidaan muutuja newItem
       newItem.setData(aData);
       if (mTail == null) { //onko jono tyhj�
    	   mTail = mHead = newItem;
       }
       else {
    	   mTail.setNext(newItem); //kytkent�
    	   mTail = newItem; // uusi alkio uudeksi h�nt�alkioksi
       }
       mSize++;
    }
    
    // palauta pinon huipulla oleva alkio, jos pinossa ei ole
    // mit��n palauta null
    public ListItem pop() {
        ListItem returned = null;
        if (mHead!= null) { //onko jonossa alkioita?
        	returned = mHead;
        	mHead = mHead.getNext();
        	mSize--;
        }
        if (mHead == null) //jos jono tyhjenee, huolehditaan ett� kumpikin p�� on tyhj�
        	mTail = null;
        return returned;
    }
    
    public int getSize() {
        return mSize;
    }
    
    
    // tulosta pinon sis�lt� muuttamatta pinoa
    public void printItems() {
       ListItem scanner = mHead;
       while (scanner != null) {
    	   System.out.print(scanner.getData()+",");
    	   scanner = scanner.getNext();
       }
    }
    
}